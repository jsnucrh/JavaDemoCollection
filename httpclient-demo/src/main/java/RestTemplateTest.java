import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @Author: njitzyd
 * @Date: 2020/8/2 11:19
 * @Description: RestTemplateTest
 * @Version 1.0.0
 */
public class RestTemplateTest {

    public static void main(String[] args) {
        new RestTemplateTest().testRestTemplate();
    }

    /**
     * 测试默认的实现
     */
    public void testRestTemplate(){
        ResponseEntity<String> forEntity = getRestTemplate().getForEntity("http://www.baidu.com", String.class);
        System.out.println(forEntity.toString());

        // 还可以有很多方法，这些方法可供参考，没有把他们具体实现，大家喜欢的可以自己再去实现
        //1. 简单Get请求
//        String result = restTemplate.getForObject(rootUrl + "get1?para=my", String.class);
//        System.out.println("简单Get请求:" + result);

        //2. 简单带路径变量参数Get请求
//        result = restTemplate.getForObject(rootUrl + "get2/{1}", String.class, 239);
//        System.out.println("简单带路径变量参数Get请求:" + result);

        //3. 返回对象Get请求（注意需包含compile group: 'com.google.code.gson', name: 'gson', version: '2.8.5'）
//        ResponseEntity<Test1> responseEntity = restTemplate.getForEntity(rootUrl + "get3/339", Test1.class);
//        System.out.println("返回:" + responseEntity);
//        System.out.println("返回对象Get请求:" + responseEntity.getBody());

        //4. 设置header的Get请求
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("token", "123");
//        ResponseEntity<String> response = restTemplate.exchange(rootUrl + "get4", HttpMethod.GET, new HttpEntity<String>(headers), String.class);
//        System.out.println("设置header的Get请求:" + response.getBody());

        //5. Post对象
//        Test1 test1 = new Test1();
//        test1.name = "buter";
//        test1.sex = 1;
//        result = restTemplate.postForObject(rootUrl + "post1", test1, String.class);
//        System.out.println("Post对象:" + result);

        //6. 带header的Post数据请求
//        response = restTemplate.postForEntity(rootUrl + "post2", new HttpEntity<Test1>(test1, headers), String.class);
//        System.out.println("带header的Post数据请求:" + response.getBody());

        //7. 带header的Put数据请求
        //无返回值
//        restTemplate.put(rootUrl + "put1", new HttpEntity<Test1>(test1, headers));
        //带返回值
//        response = restTemplate.exchange(rootUrl + "put1", HttpMethod.PUT, new HttpEntity<Test1>(test1, headers), String.class);
//        System.out.println("带header的Put数据请求:" + response.getBody());

        //8. del请求
        //无返回值
//        restTemplate.delete(rootUrl + "del1/{1}", 332);
        //带返回值
//        response = restTemplate.exchange(rootUrl + "del1/332", HttpMethod.DELETE, null, String.class);
//        System.out.println("del数据请求:" + response.getBody());

    }


    public static RestTemplate getRestTemplate(){

        RestTemplate restTemplate = new RestTemplate();
        // 如果想要修改默认的实现方案，可以在new 的时候给他一个初始化的对象
        // 或者设置setRequestFactory方法来改变
//        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
//        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        return restTemplate;
    }
}
