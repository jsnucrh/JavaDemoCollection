package com.njit.swagger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: njitzyd
 * @Date: 2020/8/2 17:45
 * @Description: DemoController
 * @Version 1.0.0
 */
@Api("DemoController 的入口")
@RestController
public class DemoController {

    /**
     *
     * @description: 注意这种写法Swagger 会生成很多个接口（如果参数里没指定GET）
     * 所以在实际的开发中尽量使用 @GetMapping 等这种指定了方法的方式
     * @param: []
     * @return: java.lang.String
     * @date: 2020/8/2
     */
    @ApiOperation("这是我的index方法")
    @RequestMapping(value = "index",method = RequestMethod.GET)
    public String hello(){
        return "helloWorld";
    }
}
